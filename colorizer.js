var math = require('mathjs');
var natural = require('natural');
var constants = require('./constants');
var coloring_functions = require('./coloring_functions');
var errors = require('./errors');
var Promise = require('promise');
var Server = require('./server');
/**
 * Adding a tokenize function to the String class
 * @method tokenize
 * @argument none
 * @return none
 **/
// TODO Need to decide how to compute colors for words, sentences, paragraphs
// etc based on the individual letters in the above.
String.prototype.tokenize = function(output) {
	switch(output) {
	case 'letters':	return this.split('');
		break;
	case 'words': return this.split(' ');
		break;
	case 'sentences': return this.split('.');
		break;
	case 'paragraphs': return this.split(new RegExp('[\r\n]+','gi'));
		break;
	default: return this.split('');
		break;
	}
}

/**
 * Takes the input text and colorizes it to show a pattern
 * @method colorize
 * @argument {String} text is a big text paragraph or sentence
 **/
var breakdown_text = function(text, separator) {
	var tokens = text.tokenize(separator);
	return tokens;
};

var colorize = function(tokens) {
	return tokens.map(function(token) {
		return color(token);
	});
};

var buildResponse = function(text, separator) {
	return Promise.all(colorize(breakdown_text(text, separator)));
}

/**
 * Returns the color of the letter or character being passed
 * @method color
 * @argument {String} string is the character that needs to be translated into color
 **/
function color(string) {
	var COLORING_FUNC = "monochrome";
	return {
		character: string,
		color: coloring_functions.colorFunc(string, COLORING_FUNC)
	};
}

exports.buildResponse = buildResponse;
