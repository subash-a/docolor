var fs = require('fs');
var errors = require('./errors');

/**
 * The method extracts the text from the file given.
 * @method getTextFromFile
 * @parameter {String} filename is the filename to be read
 * @return {Object} returns a promise which can be resolved in the future.
**/
var getTextFromFile = function(filename) {
    var p = new Promise(function(resolve, reject) {
        fs.readFile(filename, function(err, data) {
            if (err) {
                reject(errors.raiseException(err));
            }
            else {
                resolve(data.toString());
            }
        })
    });
    return p;
}

var createTextCanvas = function(colorized) {
    var p = new Promise(function(resolve, reject) {
		//options: stripe, pill, square, dot, line
		var representation = 'unit pill';
        var canvas = createCanvasStyle() + '<div class=\'container\'>' + colorized.map(makeUnit).join('\n') + '</div>';
        resolve(canvas);
    });
    return p;
};

var createCanvasStyle = function() {
	return '<style>.unit.stripe {width:100%;height:5px;} .unit.pill {width:15px;border:1px solid black;border-radius:5px;height:5px;} .unit.square {width:5px;height:5px;border: 1px solid black;} .unit.dot {width:6px;height:6px;border:1px solid black;border-radius:10px;} .unit.line {width:100%;height:1px;}</style>'
};
/**
 * Generates the required output from the array of objects
 * containing the color and character conversion
 * @method generateOutput
 * @parameter {Array} colors is the array of objects containing
 * items which will be rendered
 * @return {Array} converted array of markups from objects.
**/
var generateOutput = function(colors) {
    var config = {"type":"span"};
    return colors.map(function(item){
	return makeUnit(config, item);
    });
};

/**
 * Makes a unit of renderable text on the html document
 * @method makeUnit
 * @parameter {Object} config contains the type of element to use
 * @parameter {Object} object contains the color and character
 * @return {String} markup containing the color and character
 **/
var makeUnit = function(configobj) {
	return '<div class=\'unit pill\' style=\'background-color:#' + configobj.color + ';\' title=\'' + configobj.character + '\'></div>';
};

exports.getTextFromFile = getTextFromFile;
exports.createTextCanvas = createTextCanvas;
