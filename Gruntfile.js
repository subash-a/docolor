module.exports = function(grunt) {
grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jsdoc: {
	dist: {
	    src: ['*.js'],
	    options: {
		destination: 'docs'
	    }
	}
    }
});

grunt.loadNpmTasks('grunt-jsdoc');

grunt.registerTask('default',['jsdoc'])

};
