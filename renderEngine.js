var fs = require('fs');

/**
 * This object is the configuration of the rendering engine
 * @attribute {Object} engineConfiguration
 **/
 
var engineConfiguration = {
    'extension': 'tpl',
    'render': function(path, options, callback) {
	fs.readFile(path, function(err, data){
	    if(err) { callback(new Error('error rendering'))}
	    var rendered = data.toString()
		.replace('#colorized#',options.colorized.join(''));
	    return callback(null, rendered);
	});
    }
};

exports.configuration = engineConfiguration;
