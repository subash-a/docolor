var handleError = function(error) {
    console.log(error);
};

var raiseException = function(message) {
    return new Error(message);
};

exports.handleError = handleError;
exports.raiseException = raiseException;
