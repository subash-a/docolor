var math = require('mathjs');

var utilities = require('./utility_functions');

/**
 * A static map of colors
 * @const COLOR_MAP
 **/
var COLOR_MAP = [
    {
		'character':'A','color':'f0f0f0'
	},
	{
		'character':'B','color':'f0f0f0'
	},
	{
		'character':'C','color':'f0f0f0'
	},
	{
		'character':'D','color':'f0f0f0'
	},
	{
		'character':'E','color':'f0f0f0'
	},
	{
		'character':'F','color':'f0f0f0'
	},
	{
		'character':'G','color':'f0f0f0'
	},
	{
		'character':'H','color':'f0f0f0'
	},
	{
		'character':'I','color':'f0f0f0'
	},
	{
		'character':'J','color':'f0f0f0'
	},
	{
		'character':'K','color':'f0f0f0'
	},
	{
		'character':'L','color':'f0f0f0'
	},
	{
		'character':'M','color':'f0f0f0'
	},
	{
		'character':'N','color':'f0f0f0'
	},
	{
		'character':'O','color':'f0f0f0'
	},
	{
		'character':'P','color':'f0f0f0'
	},
	{
		'character':'Q','color':'f0f0f0'
	},
	{
		'character':'R','color':'f0f0f0'
	},
	{
		'character':'S','color':'f0f0f0'
	},
	{
		'character':'T','color':'f0f0f0'
	},
	{
		'character':'U','color':'f0f0f0'
	},
	{
		'character':'V','color':'f0f0f0'
	},
	{
		'character':'W','color':'f0f0f0'
	},
	{
		'character':'X','color':'f0f0f0'
	},
	{
		'character':'Y','color':'f0f0f0'
	},
	{
		'character':'Z','color':'f0f0f0'
	},
	{
		'character':'a','color':'f0f0f0'
	},
	{
		'character':'b','color':'f0f0f0'
	},
	{
		'character':'c','color':'f0f0f0'
	},
	{
		'character':'d','color':'f0f0f0'
	},
	{
		'character':'e','color':'f0f0f0'
	},
	{
		'character':'f','color':'f0f0f0'
	},
	{
		'character':'g','color':'f0f0f0'
	},
	{
		'character':'h','color':'f0f0f0'
	},
	{
		'character':'i','color':'f0f0f0'
	},
	{
		'character':'j','color':'f0f0f0'
	},
	{
		'character':'k','color':'f0f0f0'
	},
	{
		'character':'l','color':'f0f0f0'
	},
	{
		'character':'m','color':'f0f0f0'
	},
	{
		'character':'n','color':'f0f0f0'
	},
	{
		'character':'o','color':'f0f0f0'
	},
	{
		'character':'p','color':'f0f0f0'
	},
	{
		'character':'q','color':'f0f0f0'
	},
	{
		'character':'r','color':'f0f0f0'
	},
	{
		'character':'s','color':'f0f0f0'
	},
	{
		'character':'t','color':'f0f0f0'
	},
	{
		'character':'u','color':'f0f0f0'
	},
	{
		'character':'v','color':'f0f0f0'
	},
	{
		'character':'w','color':'f0f0f0'
	},
	{
		'character':'x','color':'f0f0f0'
	},
	{
		'character':'y','color':'f0f0f0'
	},
	{
		'character':'z','color':'f0f0f0'
	}
];

var fixedColor = function(characterIndex) {
	if(characterIndex < 90) {
		return COLOR_MAP[characterIndex - 65].color;
	}
	else {
		return COLOR_MAP[(characterIndex - 65) + 25].color;
	}
}

var grayScale = function(characterIndex) {
	var minAsciiUpperCase = 'A'.charCodeAt();
    var maxAsciiUpperCase = 'Z'.charCodeAt();
    var minAsciiLowerCase = 'a'.charCodeAt();
    var maxAsciiLowerCase = 'z'.charCodeAt();
    var charCodeRelative = characterIndex > 90 ? characterIndex - minAsciiLowerCase : characterIndex - minAsciiUpperCase;
    var maxHexValue = 255;
    var computeHexInterpolation = function(code) { return (code * maxHexValue) / (maxAsciiUpperCase - minAsciiUpperCase) };
    var hexValue = Number.parseInt(computeHexInterpolation(charCodeRelative), 10).toString(16);
    var minColor = '0x000000';
    var charColor = (Number.parseInt(minColor, 16) + Number.parseInt(hexValue, 16)).toString(16);
    var colorValue = charColor+charColor+charColor;
	return colorValue;
}

var monochromeColor = function(characterIndex, majorColor) {
    var minAsciiUpperCase = 'A'.charCodeAt();
    var maxAsciiUpperCase = 'Z'.charCodeAt();
    var minAsciiLowerCase = 'a'.charCodeAt();
    var maxAsciiLowerCase = 'z'.charCodeAt();
    var charCodeRelative = characterIndex > 90 ? characterIndex - minAsciiLowerCase : characterIndex - minAsciiUpperCase;
    var maxHexValue = 255;
    var computeHexInterpolation = function(code) { return (code * maxHexValue) / (maxAsciiUpperCase - minAsciiUpperCase) };
    var hexValue = Number.parseInt(computeHexInterpolation(charCodeRelative), 10).toString(16); //0x00ff00;
    var minColor = '0x000000';
    var charColor = (Number.parseInt(minColor, 16) + Number.parseInt(hexValue, 16)).toString(16);
    var colorValue = '0000' + charColor;
    switch(majorColor) {
	case 'RED': return utilities.RED_MAJOR(colorValue);
		break;
	case 'GREEN': return utilities.GREEN_MAJOR(colorValue);
		break;
	case 'BLUE':return utilities.BLUE_MAJOR(colorValue);
		break;
	default: return utilities.BLUE_MAJOR(colorValue);
		break;
	}
}

var spectrumColor = function(characterIndex) {
    var minAsciiUpperCase = 'A'.charCodeAt();
    var maxAsciiUpperCase = 'Z'.charCodeAt();
    var minAsciiLowerCase = 'a'.charCodeAt();
    var maxAsciiLowerCase = 'z'.charCodeAt();
    var charCodeRelative = characterIndex > 90 ? characterIndex - minAsciiLowerCase : characterIndex - minAsciiUpperCase;
    var maxHexValue = 0xffffff;
    var computeHexInterpolation = function(code) { return (code * maxHexValue) / (maxAsciiUpperCase - minAsciiUpperCase) };
    var hexValue = Number.parseInt(computeHexInterpolation(charCodeRelative), 10).toString(16);
    var minColor = '0x000000';
	var charColor = (Number.parseInt(minColor, 16) + Number.parseInt(hexValue, 16)).toString(16);
	return charColor;
}

var determine_token_length = function(token) {
	return token.length;
};

/**
 * A coloring function for interpolating between colors
 * @method colorFunc
 * @argument {String} character is a single character
 * @return {String} A hexadecimal value of the character
 **/
var colorFunc = function(token, coloringFunction) {
	var length = determine_token_length(token);
	if(length === 1) {
		return color_character(token, coloringFunction);
	}
	else if (length > 1) {
		return color_tokens(token, coloringFunction);
	}
	else {
		return 0x000000;
	}
};

// Use this function when splitting by words or sentences or higher level group.
var color_tokens = function(tokens, coloringFunction) {
	var SPACE = 32;
	var only_char = new RegExp('[A-Za-z]','gi');
	var character_array = tokens.split('');
	//console.log(character_array.filter(function(char){return only_char.test(char)}));
	var character_codes = character_array
		.filter(function(char){return only_char.test(char)})
		.map(function(char){return char.charCodeAt()});
	var avg_character_index = math.mean.apply(this, character_codes);
	var character_from_avg = String.fromCharCode(parseInt(avg_character_index,10));
	var color_code = color_character(character_from_avg, coloringFunction)
	return color_code.toString(16);
};

var color_character = function(character, coloringFunction) {
	var characterIndex = character.charCodeAt();
	switch(coloringFunction) {
	case 'monochrome': return monochromeColor(characterIndex, 'GREEN');
		break;
	case 'spectrum': return spectrumColor(characterIndex);
		break;
	case 'grayscale': return grayScale(characterIndex);
		break;
	case 'fixed': return fixedColor(characterIndex);
		break;
	default: return spectrumColor(characterIndex);
		break;
	}
};

exports.colorFunc = colorFunc;
