var leftShift = function(value, times) {
    var count = 0;
    var valueArray = value.split('');
    while (count < times) {
        valueArray.push(valueArray.shift());
        count++;
    }
	return valueArray.join('');
};

var RED_MAJOR = function(value) {return leftShift(value, 4)};
var GREEN_MAJOR = function(value) {return leftShift(value, 2)};
var BLUE_MAJOR = function(value) {return value};
var SHIFT_ONE = function(value) {return leftShift(value, 1)};
var SHIFT_THREE = function(value) {return leftShift(value, 3)};
var SHIFT_FIVE = function(value) {return leftShift(value, 5)};

exports.RED_MAJOR = RED_MAJOR;
exports.BLUE_MAJOR = BLUE_MAJOR;
exports.GREEN_MAJOR = GREEN_MAJOR;
