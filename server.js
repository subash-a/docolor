var express = require('express');
var parser = require('body-parser');
var multer = require('multer');

var colorizer = require('./colorizer');
var processor = require('./processor');
var errors = require('./errors');

var appServer = express();

var UPLOAD_PATH = './uploads/';
var STATIC_FOLDER = 'public';

appServer.use(parser.urlencoded({extended: false}));
appServer.use(multer({dest: UPLOAD_PATH}));
appServer.use(express.static(STATIC_FOLDER));


appServer.get('/', function(req, res) {
    res.send('Hello! Welcome to colorize');
});

appServer.post('/colorize', function(req, res) {
    console.log(req.body);
    console.log(req.files);

    var sendColors = function(colors) {
        sendResponse(colors);
    };

    var sendCanvas = function(colors) {
        processor.createTextCanvas(colors)
			.then(sendResponse)
			.catch(errors.handleError);
    };

    var sendResponse = function(response) {
        res.send(response);
    };

    var colorizeText = function(text) {
		var SEPARATOR = 'paragraphs';
        colorizer.buildResponse(text, SEPARATOR)
			.then(sendCanvas)
			.catch(errors.handleError);
    };

    if (req.files['uploadfile']) {
        var filepath = UPLOAD_PATH + req.files['uploadfile'].name;
        processor.getTextFromFile(filepath)
			.then(colorizeText)
			.catch(errors.handleError);
    }

    if (req.body['uploadtext']) {
        colorizeText(req.body['uploadtext']);
    }
});

var startServer = function() {
    appServer.listen(3000, function() {
        console.log('Server Started');
    });
};

exports.start = startServer;
