var Server = require('./server');
/**
 * The Main function where the application starts from
 * @method __main__
 * @argument none
 * @return none
 **/
function __main__() {
    Server.start();
}

__main__();

