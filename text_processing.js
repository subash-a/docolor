var WNdb = require('WNdb');
var Natural = require('natural');
var Promise = require('promise');

var processWord = function(word) {
	var wordnet = new natural.WordNet();
	var promise = new Promise(resolve, reject) {
		wordnet.lookup(word, resolve(results), reject(throw new Error('word not found')));
	};
	return promise;
}

exports.processWord = processWord;
